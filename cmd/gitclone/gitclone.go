package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

type repoDesc struct {
	domain string
	team   string
	repo   string
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("incorrect number of args. expected 1")
		os.Exit(1)
	}

	rd := parseRepoString(os.Args[1])
	srcDir := getSrcDir()
	cloneCMD := rd.cloneCmdForSrcDir(srcDir)
	runCmd(cloneCMD)
}

func getSrcDir() string {
	srcDir := os.Getenv("GOPATH")
	if srcDir == "" {
		srcDir = "~/code"
	} else {
		fmt.Printf("Using GOPATH as code directory [%v]\n", srcDir)
	}
	srcDir = srcDir + "/src"

	return srcDir
}

func parseRepoString(r string) *repoDesc {

	parts := strings.Split(r, "/")
	if len(parts) != 3 {
		fmt.Println("repo in incorrect format; expcted `bitbucket.org/wg/myrepo`")
		os.Exit(1)
	}

	rd := repoDesc{}
	rd.domain = parts[0]
	rd.team = parts[1]
	rd.repo = parts[2]

	return &rd
}

func (rd repoDesc) cloneCmdForSrcDir(srcDir string) string {
	cloneCMD := fmt.Sprintf(
		"git clone git@%v:%v/%v.git %v/%v/%v/%v",
		rd.domain,
		rd.team,
		rd.repo,
		srcDir,
		rd.domain,
		rd.team,
		rd.repo,
	)

	return cloneCMD
}

func runCmd(c string) {
	cmd := exec.Command("sh", "-c", c)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	cmd.Start()
	cmd.Wait()
}
